package com.kalian.bmc.bhabinm.activity;

import android.os.Bundle;
import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog.Builder;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.DialogInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.pm.PackageManager;

import com.kalian.bmc.bhabinm.fragment.TimelineFragment;
import com.kalian.bmc.bhabinm.fragment.SubmissionBottomDialogFragment;

import com.kalian.bmc.bhabinm.R;



public class FragmentsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    private static final String TAG = "ActivityManager";
    private static final String APP = "BHABINM - FragmentsActivity - ";

    /* Permission */
    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 10;
    private static String[] PERMISSIONS_STORAGE = {
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static final int ACCESS_LOCATION_PERMISSION_CODE = 100;
    private static final int READ_STORAGE_PERMISSION_CODE  = 201;
    private static final int WRITE_STORAGE_PERMISSION_CODE  = 202;


    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Checkup Permission */
        /** Location Permission First */
        verifyLocationPermission(this);

        setContentView(R.layout.activity_fragments);

        setupToolbar();

        tabLayout = (TabLayout) findViewById(R.id.widget_tablayout);

        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.timeline_grey));
        // tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.message_grey));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.graph_grey));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.pager);

        FragmentsPager adapter = new FragmentsPager(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }


    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.widget_toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        Log.i(TAG, APP +  "onTabSelected: " + tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    /*
    public void selectFrag(View view) {
         Fragment fr;
         fr = new TimelineFragment();

         FragmentManager fm = getSupportFragmentManager();
         FragmentTransaction fragmentTransaction = fm.beginTransaction();
         fragmentTransaction.replace(R.id.fragment_place, fr);
         fragmentTransaction.commit();

    }
    */

    // public void addSubmission(){

    //   SubmissionBottomDialogFragment myDialog = new SubmissionBottomDialogFragment();

    //   FragmentManager fm = getSupportFragmentManager();
    //   myDialog.show(fm, "test");

    // }


    public void selectNewSubmissionDDS(View v){
        Intent intent = new Intent(FragmentsActivity.this,
                SubmissionDDSActivity.class);
        startActivity(intent);
    }

    public void selectNewSubmissionProblemsolving(View v){

    }

    public void selectNewSubmissionYanpol(View v){

    }

    public void selectNewSubmissionDetdin(View v){

    }

    public void selectNewSubmissionGiatdesa(View v){

    }

    public void selectNewSubmissionBinfkpm(View v){

    }

    public void selectNewSubmissionSiskamling(View v){

    }

    /* Permission */
    public static void verifyLocationPermission(AppCompatActivity activity){
        int locationPermission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_LOCATION_PERMISSION_CODE);
        }

    }

    public static void verifyStoragePermissions(AppCompatActivity activity) {
        int writePermission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED ||
                readPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
                    );
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_LOCATION_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, APP + "Location Permission has been granted by user");

                    /* check for storage after location */
                    verifyStoragePermissions(this);

                } else {
                    Log.i(TAG, APP +  "Location Permission has been denied by user");
                }
                return;
            }
            case REQUEST_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, APP +  "Storage Permission has been granted by user");
                } else {
                    Log.i(TAG, APP +  "Storage Permission has been denied by user");
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
