package com.kalian.bmc.bhabinm.activity;

import android.support.v7.app.AppCompatActivity;
//import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.os.Bundle;

// [START IMPORT endpoint-client]
// import com.google.api.client.extensions.android.http.AndroidHttp;
// import com.google.api.client.extensions.android.json.AndroidJsonFactory;
// import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
// import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import java.io.IOException;
// [END]

// [START IMPORT our_bussiness_logic]
// import com.example.echo.echo.Echo;
// import com.example.echo.echo.model.Message;
// import com.example.echo.echo.model.Email;
// [END]

import com.kalian.bmc.bhabinm.service.LocationService;
import com.kalian.bmc.bhabinm.R;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "ActivityManager";
    private static final String APP = "BHABINM";

    String nrp;
    String passkey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //findViewById(R.id.login_button).setOnClickListener(this);
        Button send_bn = (Button) findViewById(R.id.login_button);
        send_bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String personNameString = "abah@jambu.com";
                Log.i(TAG, APP + ": OnClick" );
                // new EndpointsAsyncTask().execute(personNameString);
                // Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                Intent intent = new Intent(LoginActivity.this, FragmentsActivity.class);
                //Intent intent = new Intent(LoginActivity.this, TabDemoActivity.class);

                /** StartingUp Services */
                // Intent service_intent = new Intent(LoginActivity.this, LocationService.class);
                // startService(service_intent);

                //Intent intent = new Intent(LoginActivity.this, SubmissionDDSActivity.class);
                //Intent intent = new Intent(LoginActivity.this, LocationActivity.class);

                // finish();
                startActivity(intent);
                finish();
                //final String personNameString = personName.getText().toString();
            }
        });
    }


    /**
    private class EndpointsAsyncTask extends AsyncTask<String, Void, Email> {
        private Echo myEchoService = null;

        protected Email doInBackground(String... params) {
            Email response = null;
            Log.i(TAG, APP + ": EndpointsAsyncTask.doInBackground" );
            String result = "";
            if (myEchoService == null) {  // Only do this once
                Echo.Builder builder = new Echo.Builder(AndroidHttp.newCompatibleTransport(),
                        new AndroidJsonFactory(), null)
                    .setRootUrl("http://bmc-utara.appspot.com/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
                // end options for devappserver
                myEchoService = builder.build();
            }

            try {
                //return myEchoService.echo(params[0]).execute().getData();
                Message mymessage = new Message();
                mymessage.setMessage("OEEAAAAK!");
                Email myemail = new Email();
                myemail.setEmail(params[0]);
                //Log.i(TAG, APP + ": EndpointsAsyncTask.doInBackground--becho()" );
                Log.i(TAG, APP + ": EndpointsAsyncTask.doInBackground--submission()" );
                //myEchoService.becho(mymessage).execute();
                response = myEchoService.submission(myemail.getEmail()).execute();
            } catch (IOException e) {
                Log.w(TAG, APP + ": EndpointsAsyncTask.doInBackground--submission()" + e.getMessage(), e );
            }


            return response ;
        }

        protected void onPostExecute(Email result) {
            Log.i(TAG, APP + ": EndpointsAsyncTask.onPostExecute--result: " + result );

            //TextView responseView = (TextView) findViewById(R.id.login_text_view_nrp);
            //responseView.setText(result.getEmail());
        }
    }
    */
}
