package com.kalian.bmc.bhabinm.activity;
 
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kalian.bmc.bhabinm.fragment.TimelineFragment;
import com.kalian.bmc.bhabinm.fragment.GraphFragment;

 
/**
 * Created by 
 */
//Extending FragmentStatePagerAdapter
public class FragmentsPager extends FragmentStatePagerAdapter {
    
    //integer to count number of tabs
    int tabCount;
 
    //Constructor to the class 
    public FragmentsPager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }
 
    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs 
        switch (position) {
            case 0:
                TimelineFragment timelinefragment = new TimelineFragment();
                return timelinefragment;
            case 1:
                GraphFragment graphfragment = new GraphFragment();
                return graphfragment;
            // case 2:
                // GraphTab graphTab = new GraphTab();
                // return graphTab;
            default:
                return null;
        }
    }
 
    //Overriden method getCount to get the number of tabs 
    @Override
    public int getCount() {
        return tabCount;
    }
}
