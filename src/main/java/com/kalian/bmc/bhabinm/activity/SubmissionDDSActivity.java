package com.kalian.bmc.bhabinm.activity;

/* Java */
import java.lang.annotation.Annotation;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.InputStream;

/* Android */
import android.Manifest;
import android.os.Handler;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.MenuInflater;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.database.Cursor;
import android.content.Intent;
import android.widget.Button;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.design.widget.TextInputLayout;
import android.content.pm.PackageManager;

/* Location Service */
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

/* Retrofit */
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Multipart;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import retrofit2.converter.jaxb.JaxbConverterFactory;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/* import com.android.volley.Request; */
/* import com.android.volley.RequestQueue; */
/* import com.android.volley.Response; */
/* import com.android.volley.VolleyError; */
/* import com.android.volley.toolbox.StringRequest; */
/* import com.android.volley.toolbox.Volley; */
/* import com.google.gson.Gson; */
/* import com.google.gson.GsonBuilder; */

/* BMC */
import com.kalian.bmc.core.dds.SubmissionDDS;
import com.kalian.bmc.utils.timeline.Timeline;
import com.kalian.bmc.bhabinm.service.ReqresAPI;
import com.kalian.bmc.bhabinm.R;

public class SubmissionDDSActivity extends AppCompatActivity {

    private static final String TAG = "ActivityManager";
    private static final String APP = "BHABINM - SubmissionDDSActivity - ";
    private static final SubmissionDDS submissiondds = new SubmissionDDS();

    private String BMCAPI;

    /* Retrofit stuff */
    private static Retrofit retrofit;

    /* Get Permissions */
    // Storage Permissions variables
    private static String[] PERMISSIONS_STORAGE = {
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 100;
    private static final int READ_STORAGE_PERMISSION_CODE  = 101;
    private static final int WRITE_STORAGE_PERMISSION_CODE  = 102;

    private LocationManager locationManager;
    double longitudeBest, latitudeBest;

    private static int RESULT_LOAD_IMAGE = 1;

    /* Get NRP from sharedpreferenced */
    private String LoggedUserNrp ="007";

    private EditText submissiondds_lokasi_gps;
    private EditText submissiondds_tanggal;
    private EditText input_liputan_singkat;
    private EditText input_jenis_kunjungan;
    private EditText input_nama_kunjungan;
    private EditText input_alamat_kunjungan;
    private EditText input_nama_kk_kunjungan;
    private EditText input_nama_penerima_kunjungan;
    private EditText input_keterangan;
    private EditText input_pendapat_warga;
    private EditText input_temuan_kunjungan;
    private EditText input_link_fotogiat;
    private EditText input_link_fotodok;
    private ImageView view_fotodok;
    private ImageView view_fotogiat;

    private static final int FOTO_DOK = 1;
    private static final int FOTO_GIAT = 2;
    private int jenisFoto;
    private Uri path_fotogiat;
    private Uri path_fotodok;
    private String filename_fotogiat;
    private String filename_fotodok;
    private Map<String, String>  post_body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Log.i(TAG, APP + ":setupToolbar");

        setContentView(R.layout.activity_submissiondds);

        setupToolbar();

        /* Configure Themes */
        // TextInputLayout textinputlayout = (TextInputLayout) findViewById(R.id.input_layout_giat);
        // textinputlayout.setBoxStrokeColor(ContextCompat.getColor(getApplicationContext(), R.color.primary_text));

        /* attach components */
        BMCAPI = getString(R.string.backend_api);

        submissiondds_lokasi_gps = (EditText)findViewById(R.id.submissiondds_lokasi_gps);
        submissiondds_tanggal = (EditText)findViewById(R.id.submissiondds_tanggal);
        input_liputan_singkat = (EditText)findViewById(R.id.input_liputan_singkat);
        input_jenis_kunjungan = (EditText)findViewById(R.id.input_jenis_kunjungan);
        input_nama_kunjungan  =  (EditText)findViewById(R.id.input_nama_kunjungan);
        input_alamat_kunjungan = (EditText)findViewById(R.id.input_alamat_kunjungan);
        input_nama_kk_kunjungan = (EditText)findViewById(R.id.input_nama_kk_kunjungan);
        input_nama_penerima_kunjungan = (EditText)findViewById(R.id.input_nama_penerima_kunjungan);
        input_keterangan = (EditText)findViewById(R.id.input_keterangan);
        input_pendapat_warga =  (EditText)findViewById(R.id.input_pendapat_warga);
        input_temuan_kunjungan = (EditText)findViewById(R.id.input_temuan_kunjungan);
        input_link_fotogiat = (EditText)findViewById(R.id.input_link_fotogiat);
        input_link_fotodok = (EditText)findViewById(R.id.input_link_fotodok);
        view_fotodok = (ImageView)findViewById(R.id.view_fotodok);
        view_fotogiat = (ImageView)findViewById(R.id.view_fotogiat);

        /* Set Default Form Values */

        /* Get Today */
        final Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        submissiondds_tanggal = (EditText)findViewById(R.id.submissiondds_tanggal);
        submissiondds_tanggal.setText(ft.format(date));

        // Button buttonLoadImage = (Button) findViewById(R.id.buttonLoadPicture);
        // buttonLoadImage.setOnClickListener(new View.OnClickListener() {

        //   @Override
        //   public void onClick(View arg0) {

        //     Intent i = new Intent(
        //       Intent.ACTION_PICK,
        //       android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        //     startActivityForResult(i, RESULT_LOAD_IMAGE);

        //   }
        // });

        /** Location Permission handled by FragmentsActivity - snippet*/
        /* request permission */
        // ActivityCompat.requestPermissions(this,
        //         new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
        //         MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(this, "Penjelasan lokasi " , Toast.LENGTH_LONG).show();

            } else {

                // No explanation needed, we can request the permission.

                // ActivityCompat.requestPermissions(thisActivity,
                //     new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                //     MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
                Toast.makeText(this, "Request permission lokasi " , Toast.LENGTH_LONG).show();

            }
        }

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        /* Query Location */

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);
        if(provider != null) {
            locationManager.requestLocationUpdates(provider, 2 * 60 * 1000, 10, locationListenerBest);
            Toast.makeText(this, "Lokasi disediakan oleh " + provider, Toast.LENGTH_LONG).show();
        }

        if(isReadStorageAllowed()){
            Toast.makeText(this, "Akses Penyimpanan Diizinkan." , Toast.LENGTH_LONG).show();
        }



    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                    "use this app")
            .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            })
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        dialog.show();
    }

    private void setupToolbar(){
        Log.i(TAG, APP + ":setupToolbar");
        Toolbar toolbar = (Toolbar) findViewById(R.id.widget_toolbar);

        ImageButton done_button= (ImageButton) findViewById(R.id.btn_done);
        done_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, APP + "Kirim");
                        /* Storage Permission */
                        checkStoragePermission();
                        postSubmission();
                    }
                }
        );

        ImageButton cancel_button= (ImageButton) findViewById(R.id.btn_cancel);
        cancel_button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SubmissionDDSActivity.this, FragmentsActivity.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);
                    }
                }
        );

        setSupportActionBar(toolbar);
        // final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_checkmark_50);
        //ab.setDisplayHomeAsUpEnabled(false);
        //ab.setDisplayShowCustomEnabled(true);
    }

    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
    }

    private void postSubmission(){
        submissiondds.setId(0);
        submissiondds.setNrp("008");
        submissiondds.setTanggal(submissiondds_tanggal.getText().toString());
        submissiondds.setLiputanSingkat(input_liputan_singkat.getText().toString());
        submissiondds.setJenisKunjungan(input_jenis_kunjungan.getText().toString());
        submissiondds.setNamaKunjungan(input_nama_kunjungan.getText().toString());
        submissiondds.setAlamatKunjungan(input_alamat_kunjungan.getText().toString());
        submissiondds.setNamaKKKunjungan(input_nama_kk_kunjungan.getText().toString());
        submissiondds.setNamaPenerimaKunjungan(input_nama_penerima_kunjungan.getText().toString());
        submissiondds.setKeteranganKunjungan(input_keterangan.getText().toString());
        submissiondds.setPendapatWargaKunjungan(input_pendapat_warga.getText().toString());
        submissiondds.setTemuanKunjungan(input_temuan_kunjungan.getText().toString());
        submissiondds.setLinkIdFotoGiat(input_link_fotogiat.getText().toString());
        submissiondds.setLinkIdFotoDok(input_link_fotodok.getText().toString());
        submissiondds.setLokasiGps(submissiondds_lokasi_gps.getText().toString());

        post_body = new HashMap<String, String>();
        post_body.put("id", String.valueOf(SubmissionDDSActivity.submissiondds.getId()));
        post_body.put("nrp", SubmissionDDSActivity.submissiondds.getNrp());
        post_body.put("tanggal", SubmissionDDSActivity.submissiondds.getTanggal());
        post_body.put("liputanSingkat", SubmissionDDSActivity.submissiondds.getLiputanSingkat());
        post_body.put("jenisKunjungan", SubmissionDDSActivity.submissiondds.getJenisKunjungan());
        post_body.put("namaKunjungan", SubmissionDDSActivity.submissiondds.getNamaKunjungan());
        post_body.put("alamatKunjungan", SubmissionDDSActivity.submissiondds.getAlamatKunjungan());
        post_body.put("namaKKKunjungan", SubmissionDDSActivity.submissiondds.getNamaKKKunjungan());
        post_body.put("namaPenerimaKunjungan", SubmissionDDSActivity.submissiondds.getNamaPenerimaKunjungan());
        post_body.put("keteranganKunjungan", SubmissionDDSActivity.submissiondds.getKeteranganKunjungan());
        post_body.put("pendapatWargaKunjungan", SubmissionDDSActivity.submissiondds.getPendapatWargaKunjungan());
        post_body.put("temuanKunjungan", SubmissionDDSActivity.submissiondds.getTemuanKunjungan());
        post_body.put("linkIdFotoGiat", SubmissionDDSActivity.submissiondds.getLinkIdFotoGiat());
        post_body.put("linkIdFotoDok", SubmissionDDSActivity.submissiondds.getLinkIdFotoDok());
        post_body.put("lokasiGps", SubmissionDDSActivity.submissiondds.getLokasiGps());

        // requestJsonObject(submissiondds); //volley
        retroPost(submissiondds, post_body, path_fotogiat, path_fotodok);
    }

    private void retroPost(SubmissionDDS submissiondds,
            Map<String, String> post_body,
            Uri path_fotogiat,
            Uri path_fotodok){

        /* String url = BMCAPI; */

        retrofit = new Retrofit.Builder()
            .baseUrl(BMCAPI)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        ReqresAPI reqresapi = retrofit.create(ReqresAPI.class);

        File file_fotogiat = new File(filename_fotogiat);
        /* String[] filePathColumn = { MediaStore.Images.Media.DATA };
         * Cursor cursor = getContentResolver().query(path_fotogiat,
         *         filePathColumn, null, null, null);
         * cursor.moveToFirst();
         * int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
         * String pathfoto = cursor.getString(columnIndex);
         * cursor.close(); */

        RequestBody rb_fotogiat = RequestBody.create(MediaType.parse("image/*"),
                file_fotogiat);

        File file_fotodok = new File(filename_fotodok);
        RequestBody rb_fotodok = RequestBody.create(MediaType.parse("image/*"),
                file_fotodok);

        Gson gson = new Gson();
        String json_post_body = gson.toJson(post_body);
        Log.i(TAG, APP + "Retrofit json_post_body: \n" + json_post_body);

        RequestBody rb_submissiondds = RequestBody.create(
                okhttp3.MediaType.parse("application/json; charset=utf-8"),
                json_post_body);

        MultipartBody.Part body_fotogiat = MultipartBody.Part.createFormData(
                "fotogiat", file_fotogiat.getName(), rb_fotogiat);

        MultipartBody.Part body_fotodok = MultipartBody.Part.createFormData(
                "fotodok", file_fotodok.getName(), rb_fotodok);

        // MultipartBody.Part body_submissiondds = MultipartBody.Part.create(
        //         rb_submissiondds);

        Call<ResponseBody> call = reqresapi.createSubmissionDDS(body_fotogiat,
               body_fotodok, rb_submissiondds );

        call.enqueue(
                new Callback<ResponseBody>() {
                    public void onResponse(Call<ResponseBody> call,
                            Response<ResponseBody> response){
                        Log.i(TAG, APP + "Retrofit POST: DONE");
                        if (response.isSuccessful()) {
                            try{
                                Gson gson = new Gson();
                                String json = response.body().string();
                                Log.i(TAG, APP + json);
                                Toast.makeText(SubmissionDDSActivity.this,
                                        json,
                                        Toast.LENGTH_SHORT).show();
                            } catch (IOException ioe){
                                // TODO
                            }
                        } else {
                            // Gson gson = new Gson();
                            // String errjson = gson.toJson(response);
                            Log.i(TAG, APP + "Retrofit POST: ERROR RESPONSE - " +
                                   response.code() + " - " + response.message());
                            Toast.makeText(SubmissionDDSActivity.this,
                                   "Retrofit POST: ERROR RESPONSE - " +
                                   response.code() + " - " + response.message(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i(TAG, APP + "Retrofit POST: ERROR :" + t.getMessage());
                    }
                }
        );
    }

    private void requestJsonObject(SubmissionDDS submissiondds){
        /* RequestQueue queue = Volley.newRequestQueue(this); */

        /* String url = "http://51.254.227.42/api/v1/submissiondds"; */
        /*

        String url = BMCAPI;
        String Request postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Intent intent = new Intent(SubmissionDDSActivity.this, FragmentsActivity.class);
                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        // Log.d("Error.Response", error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();

                params.put("id", String.valueOf(SubmissionDDSActivity.submissiondds.getId()));
                params.put("nrp", SubmissionDDSActivity.submissiondds.getNrp());
                params.put("tanggal", SubmissionDDSActivity.submissiondds.getTanggal());
                params.put("liputan_singkat", SubmissionDDSActivity.submissiondds.getLiputanSingkat());
                params.put("jenis_kunjungan", SubmissionDDSActivity.submissiondds.getJenisKunjungan());
                params.put("nama_kunjungan", SubmissionDDSActivity.submissiondds.getNamaKunjungan());
                params.put("alamat_kunjungan", SubmissionDDSActivity.submissiondds.getAlamatKunjungan());
                params.put("nama_kk_kunjungan", SubmissionDDSActivity.submissiondds.getNamaKKKunjungan());
                params.put("nama_penerima_kunjungan", SubmissionDDSActivity.submissiondds.getNamaPenerimaKunjungan());
                params.put("keterangan_kunjungan", SubmissionDDSActivity.submissiondds.getKeteranganKunjungan());
                params.put("pendapat_warga_kunjungan", SubmissionDDSActivity.submissiondds.getPendapatWargaKunjungan());
                params.put("temuan_kunjungan", SubmissionDDSActivity.submissiondds.getTemuanKunjungan());
                params.put("link_id_foto_giat", SubmissionDDSActivity.submissiondds.getLinkIdFotoGiat());
                params.put("link_id_foto_dok", SubmissionDDSActivity.submissiondds.getLinkIdFotoDok());
                params.put("lokasi_gps", SubmissionDDSActivity.submissiondds.getLokasiGps());

                return params;
            }
        };
        queue.add(postRequest);
        */

    }

    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeBest = location.getLongitude();
            latitudeBest = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    submissiondds_lokasi_gps.setText(
                            latitudeBest + "---" + longitudeBest );
                    Toast.makeText(SubmissionDDSActivity.this,
                            "Lokasi terkini di perbaharui",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    /* Permissions Handler */
    public void checkStoragePermission(){


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(this, "Penjelasan akses penyimpanan " , Toast.LENGTH_LONG).show();
                Log.i(TAG, APP + "Penjelasan");

            } else {
                Toast.makeText(this, "Tidak ada penjelasan akses penyimpanan" , Toast.LENGTH_LONG).show();
                Log.i(TAG, APP + "Tidak Ada Penjelasan");

                // No explanation needed, we can request the permission.

                // ActivityCompat.requestPermissions(thisActivity,
                //     new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                //     MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                /* request permission */
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_STORAGE_PERMISSION_CODE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public static void verifyStoragePermissions(AppCompatActivity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
                    );
        }
    }

    //We are calling this method to check the permission status
    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_STORAGE_PERMISSION_CODE);
    }


    /** NOT HANDLE PERMISSION HERE - PERMISSION HANDLED BY FragmentActivity */
    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.i(TAG, "Location Permission has been denied by user");

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.i(TAG, "Location Permission has been granted by user");
                }
                return;
            }
            case READ_STORAGE_PERMISSION_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Storage Permission has been denied by user");
                } else {
                    Log.i(TAG, "Storage Permission has been granted by user");
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /* Foto Handler */

    public void onAddFotoDok(View v){
        jenisFoto = FOTO_DOK;
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);
    }

    public void onAddFotoGiat(View v){
        jenisFoto = FOTO_GIAT;
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri uriselectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(uriselectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            String pathfoto = cursor.getString(columnIndex);
            cursor.close();

            Log.i(TAG, APP + " -> " + pathfoto);

            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                /* CaseOfFoto */
                String filenamefoto = pathfoto.substring(pathfoto.lastIndexOf("/")+1);
                switch (jenisFoto) {
                    case FOTO_DOK:
                        view_fotodok.setImageBitmap(selectedImage);
                        view_fotodok.setVisibility(View.VISIBLE);
                        input_link_fotodok.setText(filenamefoto);
                        /* path_fotodok = imageUri; */
                        filename_fotodok = pathfoto;
                        break;
                    case FOTO_GIAT:
                        view_fotogiat.setImageBitmap(selectedImage);
                        view_fotogiat.setVisibility(View.VISIBLE);
                        input_link_fotogiat.setText(filenamefoto);
                        /* path_fotogiat = imageUri; */
                        filename_fotogiat = pathfoto;
                        break;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            // imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }

        /*
           if (resultCode == RESULT_OK) {
           try {
           final Uri imageUri = data.getData();
           final InputStream imageStream = getContentResolver().openInputStream(imageUri);
           final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
           image_view.setImageBitmap(selectedImage);
           Toast.makeText(SubmissionDDSActivity.this, "Something went right", Toast.LENGTH_LONG).show();
           } catch (FileNotFoundException e) {
           e.printStackTrace();
           Toast.makeText(SubmissionDDSActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
           }

           }else {
           Toast.makeText(SubmissionDDSActivity.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
           }
           */

    }

    public String getUriPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }


}
