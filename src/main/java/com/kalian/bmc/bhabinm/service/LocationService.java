package com.kalian.bmc.bhabinm.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;
import android.util.Log;


/* Fuse Location */
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import com.kalian.bmc.bhabinm.R;

public class LocationService extends Service {

    private static final String TAG = "ActivityManager";
    private static final String APP = "BHABINM";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Toast.makeText(this,"Service Created",300);
        Log.i(TAG, APP + ": LocationService.onCreate()" );

        final Notification.Builder builder =
        new Notification.Builder(this)
        .setSmallIcon(R.drawable.logo_notif)
        .setContentTitle("Bhabin Mobile")
        .setContentText("Background Service...");

        final Notification notification = builder.build();

        startForeground(1, notification);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Toast.makeText(this,"Service Destroy",300);
        Log.i(TAG, APP + ": LocationService.onDestroy()" );
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // Toast.makeText(this,"Service LowMemory",300);
        Log.i(TAG, APP + ": LocationService.onLowMemory()" );

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        // Toast.makeText(this,"Service start",300);
        Log.i(TAG, APP + ": LocationService.onStart()" );

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        
        // Toast.makeText(this,"task perform in service",300);
        Log.i(TAG, APP + ": LocationService.onStartCommand()" );

        ThreadDemo td = new ThreadDemo();
        td.start();

        return super.onStartCommand(intent, flags, startId);
    }
    
    private class ThreadDemo extends Thread{
        @Override
        public void run() {
            super.run();
            try{
                sleep(7*1000);    
                handler.sendEmptyMessage(0);
                Log.i(TAG, APP + "NOTIF: Lat 0 Long 0" );
            }catch(Exception e){
                e.getMessage();
            }
        }
    }
   private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //showAppNotification();
        }
   };
}