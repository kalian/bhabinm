package com.kalian.bmc.bhabinm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Multipart;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;


import com.kalian.bmc.utils.timeline.Timeline;

public interface ReqresAPI {

    // @GET("api/users/{id}")
    // Call<User> doGetUser(@Path("id") String id);

    @GET("timeline/latest")
    Call<ArrayList<Timeline>> doGetTimeline();

    // Next setup authorization
    // Call<User> editUser (@Header("Authorization") String authorization, @Part("file\"; filename=\"pp.png\" ") RequestBody file , @Part("FirstName") RequestBody fname, @Part("Id") RequestBody id);

    @Multipart
    @POST("submissiondds")
    Call<ResponseBody> createSubmissionDDS(
        @Part MultipartBody.Part filefotogiat,
        @Part MultipartBody.Part filefotodok,
        @Part("submissiondds") RequestBody submissiondds);

    // @POST("/api/users")
    // Call<User> createUser(@Body User user);

    // @GET("/api/users?")
    // Call<UserList> doGetUserList(@Query("page") String page);

    // @FormUrlEncoded
    // @POST("/api/users?")
    // Call<UserList> doCreateUserWithField(@Field("name") String name, @Field("job") String job);
}
