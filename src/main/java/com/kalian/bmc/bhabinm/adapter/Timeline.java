package com.kalian.bmc.bhabinm.adapter;

import com.google.gson.annotations.SerializedName;

public class Timeline {

	@SerializedName("id")
	private int id;
	@SerializedName("nrp")
	private String nrp;
	@SerializedName("tanggal")
	private String tanggal;
	@SerializedName("nama_anggota")
	private String nama_anggota;
	@SerializedName("avatar")
	private String avatar;
	@SerializedName("nama_aktifitas")
	private String nama_aktifitas;
	@SerializedName("liputan_singkat")
	private String liputan_singkat;
	@SerializedName("link_id_foto_giat")
    private String link_id_foto_giat;
	@SerializedName("lokasi_gps")
	private String lokasi_gps;

	public Timeline(){

	}

	public Timeline(
		int id,
		String nrp,
		String tanggal,
		String nama_anggota,
		String avatar,
		String nama_aktifitas,
		String liputan_singkat,
		String link_id_foto_giat,
		String lokasi_gps
		){
		this.id = id;
		this.nrp = nrp;
		this.tanggal = tanggal;
		this.nama_anggota = nama_anggota;
		this.avatar = avatar;
		this.nama_aktifitas = nama_aktifitas;
		this.liputan_singkat = liputan_singkat;
        this.link_id_foto_giat = link_id_foto_giat;
		this.lokasi_gps = lokasi_gps;
	}


	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return this.id;
	}

	public void setNrp(String nrp){
		this.nrp = nrp;
	}

	public String getNrp(){
		return this.nrp;
	}

	public void setTanggal(String tanggal){
		this.tanggal = tanggal;
	}

	public String getTanggal(){
		return this.tanggal;
	}

	public void setNamaAnggota(String nama_anggota){
		this.nama_anggota = nama_anggota;
	}

	public String getNamaAnggota(){
		return this.nama_anggota;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return this.avatar;
	}

	public void setNamaAktifitas(String nama_aktifitas){
		this.nama_aktifitas = nama_aktifitas;
	}

	public String getNamaAktifitas(){
		return this.nama_aktifitas;
	}

	public void setLiputanSingkat(String liputan_singkat){
		this.liputan_singkat = liputan_singkat;
	}

	public String getLiputanSingkat(){
		return this.liputan_singkat;
	}

	public void setLinkIdFotoGiat(String link_id_foto_giat){
		this.link_id_foto_giat = link_id_foto_giat;
	}
	
	public String getLinkIdFotoGiat(){
		return this.link_id_foto_giat;
	}

	public void setLokasiGps(String lokasi_gps){
		this.lokasi_gps = lokasi_gps;
	}

	public String getLokasiGps(){
		return this.lokasi_gps;
	}

}