package com.kalian.bmc.bhabinm.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.util.Log;

import com.kalian.bmc.utils.timeline.Timeline;


import com.kalian.bmc.bhabinm.R;

public class TimelineRecyclerAdapter extends RecyclerView.Adapter<TimelineRecyclerAdapter.ViewHolder> {

    private static final String TAG = "ActivityManager";
    private static final String APP = "BHABINM";

    private List<Timeline> itemList;
    private Context context;

	public TimelineRecyclerAdapter(Context context, List<Timeline> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TimelineRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                               .inflate(R.layout.row_view_timeline, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        // if(timeline!=null){
        //     viewHolder.txtViewTitle.setText(timeline[position].txtViewNamaAnggota());
        // }
        Log.i(TAG, APP + ": onBindViewHolder " );

        Log.i(TAG, APP + ": getId()" + itemList.get(position).getId() );
        Log.i(TAG, APP + ": getNrp()" + itemList.get(position).getNrp() );
        Log.i(TAG, APP + ": getTanggal()" + itemList.get(position).getTanggal() );

        viewHolder.txtViewNamaAnggota.setText(itemList.get(position).getNrp());
        // viewHolder.txtViewDetilAktifitas.setText(itemList.get(position).getTanggal());

        // viewHolder.imgViewIcon.setImageResource(timeline[position].getImageUrl());
        // viewHolder.txtViewTitle.setText(timeline[position].getLiputanSingkat();

        /*** INI MASIH WAITING ***/

    }

    // Return the size of your timeline (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtViewNamaAnggota;
        // public TextView txtViewDetilAktifitas;
        // public ImageView imgViewIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemLayoutView.setOnClickListener(this);
            txtViewNamaAnggota = (TextView) itemLayoutView.findViewById(R.id.nama_anggota);
            // imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
            // txtViewDetilAktifitas = (TextView) itemLayoutView.findViewById(R.id.item_liputan_singkat);
            //
        }

        @Override
        public void onClick(View view) {

        }

    }


}
