package com.kalian.bmc.bhabinm.adapter;

public class FeedItem {

    private String title;
    private int imageurl;

    public FeedItem(String title, int imageurl){
        this.title = title;
        this.imageurl = imageurl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageUrl() {
        return this.imageurl;
    }

    public void setImageUrl(int imageurl) {
        this.imageurl = imageurl;
    }


}