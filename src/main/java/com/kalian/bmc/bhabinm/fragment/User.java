package com.kalian.bmc.bhabinm.fragment;

import com.google.gson.annotations.SerializedName;

public class User {

	@SerializedName("id")
	private int    id;

	@SerializedName("first_name")
    private String firstname;

	@SerializedName("last_name")
    private String lastname;

	@SerializedName("avatar")
    private String avatar;

    public int getId() {
        return this.id;
    }

    public void setId(int id){
    	this.id = id;
    }

    public String getFirstname(){
    	return this.firstname;
    }

    public void setFirstname(String firstname){
    	this.firstname = firstname;
    }

    public String getLastname(){
    	return this.lastname;
    }

    public void setLastname(String lastname){
    	this.lastname = lastname;
    }

    public String getAvatar(){
    	return this.avatar;
    }

    public void setAvatar(String avatar){
    	this.avatar = avatar;
    }

}