package com.kalian.bmc.bhabinm.fragment;

import java.util.List;
import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.design.widget.FloatingActionButton;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;

import android.os.Handler;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
// import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.util.Log;

/* Retrofit */
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.converter.gson.GsonConverterFactory;
import com.google.gson.Gson;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import retrofit2.converter.jaxb.JaxbConverterFactory;


import com.kalian.bmc.bhabinm.adapter.TimelineRecyclerAdapter;

// import com.kalian.bmc.bhabinm.adapter.FeedItem;
// import com.kalian.bmc.bhabinm.adapter.Timeline;

import com.kalian.bmc.bhabinm.service.ReqresAPI;

import com.kalian.bmc.utils.timeline.Timeline;

import com.kalian.bmc.bhabinm.fragment.SubmissionBottomDialogFragment;


import com.kalian.bmc.bhabinm.R;

//Our class extending fragment
public class TimelineFragment extends Fragment {

	private static final String TAG = "ActivityManager";
    private static final String APP = "BHABINM";

    private static final String BMCAPI = "http://bmcpontura.lndyn.com/api/v1/";


    Context context;

    // Create the Handler
    private Handler handler;

	// private Timeline[] timelineList;
    private RecyclerView mRecyclerView;
    private TimelineRecyclerAdapter adapter;

    protected RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.context = this.getActivity();

        handler = new Handler();
        // Start the Runnable immediately
        handler.post(runnable);



        // Initialize dataset, this data would usually come from a local content provider or
        // remote server.
        //initDataset();
        // this is data fro recycler view
        // timelineList = new Timeline[10];
        // feedsList = new FeedItem[7];

        // feedsList[0] = new FeedItem("Help",R.drawable.placeholder);
        // feedsList[1] = new FeedItem("Help",R.drawable.placeholder);
        // feedsList[2] = new FeedItem("Help",R.drawable.placeholder);
        // feedsList[3] = new FeedItem("Help",R.drawable.placeholder);
        // feedsList[4] = new FeedItem("Help",R.drawable.placeholder);
        // requestJsonObject(context);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        makeRetrofitCalls();

    	View rootView = inflater.inflate(R.layout.fragment_timeline, container, false);
        rootView.setTag(TAG);

    	mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(getActivity());

        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setLayoutManager(mLayoutManager);

        // adapter = new TimelineRecyclerAdapter(timelineList);

        // mRecyclerView.setAdapter(adapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // requestJsonObject();

        FloatingActionButton fab = (FloatingActionButton)
            rootView.findViewById(R.id.fab_add_submission);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Initializing a bottom sheet
                SubmissionBottomDialogFragment bottomSheetDialogFragment =
                    new SubmissionBottomDialogFragment();

                //show it
                bottomSheetDialogFragment.show(getActivity()
                        .getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void requestJsonObject(){
        // RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        // String url ="http://51.254.227.42/api/v1/timeline/topten";
        // StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        //             @Override
        //             public void onResponse(String response) {
        //                 Log.d(TAG, "Response " + response);
        //                 GsonBuilder builder = new GsonBuilder();
        //                 Gson mGson = builder.create();
        //                 List<Timeline> posts = new ArrayList<Timeline>();
        //                 posts = Arrays.asList(mGson.fromJson(response, Timeline[].class));
        //                 adapter = new TimelineRecyclerAdapter(getActivity().getApplicationContext(), posts);
        //                 mRecyclerView.setAdapter(adapter);
        //             }
        //         }, new Response.ErrorListener() {
        //     @Override
        //     public void onErrorResponse(VolleyError error) {
        //         Log.d(TAG, "Error " + error.getMessage());
        //     }
        // });
        // queue.add(stringRequest);
    }


    private void makeRetrofitCalls() {
        Log.i(TAG, APP + "makeRetrofitCalls");
        Retrofit retrofit = new Retrofit.Builder()
           .baseUrl(BMCAPI) // 1
           // .addConverterFactory(SimpleXmlConverterFactory.create()) // 2
           // .addConverterFactory(JaxbConverterFactory.create()) // 2
           .addConverterFactory(GsonConverterFactory.create())
           .build();

        ReqresAPI reqresapi = retrofit.create(ReqresAPI.class); // 3

        Call<ArrayList<Timeline>> call = reqresapi.doGetTimeline(); // 4

        call.enqueue(
            new Callback<ArrayList<Timeline>>() {  // 5
                public void onResponse(Call<ArrayList<Timeline>> call,
                    Response<ArrayList<Timeline>> response){

                    // downloadComplete(response.body());  // 6
                    final ArrayList<Timeline> array_timeline = response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    Log.i(TAG, APP + "Retrofit Req DONE");
                    // Log.i(TAG, APP + json);

                    /* Recycleview */
                    adapter = new TimelineRecyclerAdapter(getActivity()
                            .getApplicationContext(), array_timeline);
                    mRecyclerView.setAdapter(adapter);
                }

                public void onFailure(Call<ArrayList<Timeline>> call,
                        Throwable t) {
                    // Toast.makeText(MainActivity.this,
                    // t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    Log.i(TAG, APP + "Retrofit Req ERROR :" + t.getMessage());
                }
            }
        );
    }

    // Define the code block to be executed
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
              // Insert custom code here
              // requestJsonObject();
            makeRetrofitCalls();
              // Repeat every 2 seconds
          handler.postDelayed(runnable, 60000);
        }
    };
    //handler.removeCallbacks(runnable); <-- to remove

}
