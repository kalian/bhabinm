package com.kalian.bmc.bhabinm.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.design.widget.FloatingActionButton;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;

import android.os.Handler;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.graphics.Color;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;


import android.util.Log;

import com.kalian.bmc.bhabinm.R;

public class GraphFragment extends Fragment {

	private static final String APP = "BHABINM";
	private static final String TAG = "GraphFragment";

    Context context;

     // Create the Handler
    private Handler handler;

    private DataPoint[] datapointlist; 

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.context = this.getActivity();
        handler = new Handler();

        // Start the Runnable immediately
        handler.post(runnable);

    }

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    	View rootView = inflater.inflate(R.layout.fragment_graph, container, false);
        rootView.setTag(TAG);


        requestJsonObject();

        // GraphView graph = (GraphView) rootView.findViewById(R.id.graph);

        // BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[] {
        // 	new DataPoint(0, 9),
        // });
        // graph.addSeries(series);

        // BarGraphSeries<DataPoint> series2 = new BarGraphSeries<>(new DataPoint[] {
        // 	new DataPoint(0, 4),
        // });
        // graph.addSeries(series2);



		// styling
        // series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
        	// @Override
        	// public int get(DataPoint data) {
        		// return Color.rgb((int) data.getX()*255/4, (int) Math.abs(data.getY()*255/6), 100);
        	// }
        // });

        // series.setSpacing(50);

		// draw values on top
        // series.setDrawValuesOnTop(true);
        // series.setValuesOnTopColor(Color.RED);
		//series.setValuesOnTopSize(50);

        // // use static labels for horizontal and vertical labels
        // StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        // staticLabelsFormatter.setHorizontalLabels(new String[] {"old", "middle", "new"});
        // staticLabelsFormatter.setVerticalLabels(new String[] {"low", "middle", "high"});
        // graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);

        //  // legend
        // series.setTitle("foo");
        // series2.setTitle("bar");
        // graph.getLegendRenderer().setVisible(true);
        // graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);


        /****** MPCHart ******/
        BarChart chart = (BarChart) rootView.findViewById(R.id.chart);
 
        BarData data = new BarData(getXAxisValues(), getDataSet());
        chart.setData(data);
        chart.setDescription("My Chart");
        chart.animateXY(2000, 2000);
        chart.invalidate();


        return rootView;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void requestJsonObject(){
        // RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        // String url ="http://51.254.227.42/api/v1/timeline/topten";
        // StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        //             @Override
        //             public void onResponse(String response) {
        //                 Log.d(TAG, "Response " + response);
        //                 GsonBuilder builder = new GsonBuilder();
        //                 Gson mGson = builder.create();
        //                 // List<Timeline> posts = new ArrayList<Timeline>();
        //                 // posts = Arrays.asList(mGson.fromJson(response, Timeline[].class));
        //                 // adapter = new TimelineRecyclerAdapter(getActivity().getApplicationContext(), posts);
        //                 // mRecyclerView.setAdapter(adapter);
        //             }
        //         }, new Response.ErrorListener() {
			     //        @Override
			     //        public void onErrorResponse(VolleyError error) {
			     //            Log.d(TAG, "Error " + error.getMessage());
			     //        }
			     //    });
        // queue.add(stringRequest);
    }


    // Define the code block to be executed
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
              // Insert custom code here
              requestJsonObject();

              // Repeat every 2 seconds
	          handler.postDelayed(runnable, 60000);
        }
    };
    //handler.removeCallbacks(runnable); <-- to remove


     private ArrayList<BarDataSet> getDataSet( ) {
        ArrayList<BarDataSet> dataSets = null;
 
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(11, 0); // Jan
        valueSet1.add(v1e1);
 
        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(15, 0); // Jan
        valueSet2.add(v2e1);

        ArrayList<BarEntry> valueSet3 = new ArrayList<>();
        BarEntry v3e1 = new BarEntry(11, 0); // Jan
        valueSet3.add(v3e1);

        ArrayList<BarEntry> valueSet4 = new ArrayList<>();
        BarEntry v4e1 = new BarEntry(13, 0); // Jan
        valueSet4.add(v4e1);
 
        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Batulayang");
        barDataSet1.setColor(Color.rgb(0, 155, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Siantan Tengah");
        barDataSet2.setColor(Color.rgb(0, 0, 155));
        BarDataSet barDataSet3 = new BarDataSet(valueSet3, "Siantan Hulu");
        barDataSet3.setColor(Color.rgb(155, 0, 0 ));
        BarDataSet barDataSet4 = new BarDataSet(valueSet4, "Siantan Hilir");
        barDataSet4.setColor(Color.rgb(055, 0, 0 ));
 
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        dataSets.add(barDataSet3);
        dataSets.add(barDataSet4);

        return dataSets;
    }
 
    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("FEB");
        return xAxis;
    }



}